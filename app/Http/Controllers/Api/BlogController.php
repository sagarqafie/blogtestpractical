<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\blogs;
use Validator;

class BlogController extends Controller
{
   /**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
    public function index()
    {
        $blogs = blogs::all();

        $blogData = array();
        $blogArray = array();
         foreach($blogs as $blog){
            $blogArray['name'] = $blog['name'];
            $blogArray['description'] = $blog['description'];
            $blogArray['author'] = $blog['author'];
            if($blog['image'] != ''){
                $blogArray['image'] = url(Storage::url($blog['image']));    
            }
            $blogArray['created_at'] = date('Y-m-d', strtotime($blog['created_at']));
            $blogData[] = $blogArray;
         }   

        return response()->json([
        "success" => true,
        "message" => "Blogs List",
        "data" => $blogData
        ]);
    }


        /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
        public function store(Request $request)
        {
            $input = $request->all();
            $validator = Validator::make($input, [
                                                    'name' => 'required',
                                                    'description' => 'required',
                                                    'author' => 'required'
                                        ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" =>  $validator->errors(),
                    "data" => []
                    ]);
              #  return $this->sendError('Validation Error.', $validator->errors());       
            }

            
            if(isset($input['image']) && $input['image'] != ''){
                $image_64 = $input['image']; //your base64 encoded data

                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

                $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

                // find substring fro replace here eg: data:image/png;base64,

                $image = str_replace($replace, '', $image_64); 

                $image = str_replace(' ', '+', $image); 

                $imageName = Str::random(10).'.'.$extension;

                $input['image'] = $imageName; 

                Storage::disk('public')->put($imageName, base64_decode($image));
            }

             

            $blog = blogs::create($input);
            return response()->json([
            "success" => true,
            "message" => "Blog created successfully.",
            "data" => $blog
            ]);
        }
        
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
            $blog = blogs::find($id);
            if (is_null($blog)) {
                return response()->json([
                    "success" => false,
                    "message" =>  "Blog Not Found!",
                    "data" => []
                    ]);
            }

            $blog['image'] = url(Storage::url($blog['image']));  
            return response()->json([
            "success" => true,
            "message" => "Blog retrieved successfully.",
            "data" => $blog
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, blogs $blogs)
    {

        
            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => 'required',
                'description' => 'required',
                'author' => 'required'
            ]);
            if($validator->fails()){
                return response()->json([
                    "success" => false,
                    "message" =>   $validator->errors(),
                    "data" => []
                    ]);
            }

            if(isset($input['image']) && $input['image'] != ''){
                $image_64 = $input['image']; //your base64 encoded data

                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf

                $replace = substr($image_64, 0, strpos($image_64, ',')+1); 

                // find substring fro replace here eg: data:image/png;base64,

                $image = str_replace($replace, '', $image_64); 

                $image = str_replace(' ', '+', $image); 

                $imageName = Str::random(10).'.'.$extension;

                $blogs->image = $imageName; 

                Storage::disk('public')->put($imageName, base64_decode($image));
            }
            $blogs->name = $input['name'];
            $blogs->description = $input['description'];
            $blogs->author = $input['author'];
            $blogs->save();
            return response()->json([
            "success" => true,
            "message" => "Blog updated successfully.",
            "data" => $blogs
            ]);
    }
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $blogs = blogs::find($id);
        if(!empty($blogs)){
            $imageName = $blogs['image'];
            if($blogs->delete()){
    
                    if($imageName != ''){
                        unlink(Storage::url($blog['image']));
                    }
    
                        return response()->json([
                                "success" => true,
                                "message" => "Blog deleted successfully.",
                                "data" => []
                        ]);
                }else{
                    return response()->json([
                            "success" => false,
                            "message" => "Try Again.",
                            "data" => $blogs
                    ]);
                }
        }else{
            return response()->json([
                    "success" => false,
                    "message" => "This blog ID is not exists!",
                    "data" => $blogs
            ]);
        }
       
    }
}
